import RomanNumerals from "../../src/katas/RomanNumerals";

describe("RomanNumerals", () => {

    it("can transform", () => {
        expect(RomanNumerals.toRoman(1)).toBe("I");
        expect(RomanNumerals.toRoman(2)).toBe("II");
        expect(RomanNumerals.toRoman(3)).toBe("III");
        expect(RomanNumerals.toRoman(4)).toBe("IV");
        expect(RomanNumerals.toRoman(5)).toBe("V");
        expect(RomanNumerals.toRoman(6)).toBe("VI");
        expect(RomanNumerals.toRoman(7)).toBe("VII");
        expect(RomanNumerals.toRoman(8)).toBe("VIII");
        expect(RomanNumerals.toRoman(9)).toBe("IX");

        expect(RomanNumerals.toRoman(1987)).toBe("MCMLXXXVII");
    });
});

//http://securesoftwaredev.com/2011/12/05/practicing-tdd-using-the-roman-numerals-kata/