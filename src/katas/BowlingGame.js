class BowlingGame {

    constructor() {
        this.rolls = [];
        for (var i = 0; i < 22; i++) {
            this.rolls.push(0)
        }
        this.currentRoll = 0;
    }

    roll(pins) {
        this.rolls[this.currentRoll] = pins;
        this.currentRoll++;
    }

    getScore() {
        var score = 0;
        var firstInFrame = 0;
        for (var frame = 0; frame < 10; frame++) {
            if (this._isStrike(firstInFrame)) {
                score += 10 + this._nextTwoBallsForStrike(firstInFrame);
                firstInFrame += 1;
            } else if (this._isSpare(firstInFrame)) {
                score += 10 + this._nextBallForSpare(firstInFrame);
                firstInFrame += 2;
            } else {
                score += this._twoBallsInFrame(firstInFrame);
                firstInFrame += 2;
            }
        }
        return score;
    }

    _twoBallsInFrame(firstInFrame) {
        return this.rolls[firstInFrame] + this.rolls[firstInFrame + 1];
    }

    _nextTwoBallsForStrike(firstInFrame) {
        return this.rolls[firstInFrame + 1] + this.rolls[firstInFrame + 2];
    }

    _nextBallForSpare(firstInFrame) {
        return this.rolls[firstInFrame + 2];
    }

    _isSpare(i) {
        return this.rolls[i] + this.rolls[i + 1] == 10;
    }

    _isStrike(i) {
        return this.rolls[i] == 10;
    }
}

export default BowlingGame