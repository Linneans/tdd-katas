# Getting started

## npm

In the terminal execute the following command:

```$ npm install```

## IntelliJ

### Configure Wallaby

Open:

```
Run -> Edit Configurations..
```

Then add new configuration by clicking ```+```, name it Wallaby, and browse for the ```wallaby.js``` file that resides in the root directory of the repository.
 
## Setting correct ECMAScript version

Open any JavaScript Spec file, e.g. ```BowlingGameSpec.js``` place the marker at the row ```import BowlingGame from <path>```, press ```alt + enter```, select the suggested version and press ```enter``` again.